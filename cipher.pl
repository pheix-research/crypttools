#!/usr/bin/env perl6

# NOTE:
# key (256 bits, 32 bytes) - key for aes256
# iv (128 bits, 16 bytes) - initialization vector: https://stackoverflow.com/questions/39412760/what-is-an-openssl-iv-and-why-do-i-need-a-key-and-an-iv

use v6;
use OpenSSL::CryptTools;
use OpenSSL::Digest;
use experimental :pack;

# universal func for iv/key generation
sub gen_cipher_param( $pname ) {
    my $iv = Nil;
    my $key = '72<84d5e~98#&a61^!(5324f7#0a5!@r';
    if $pname eq 'iv' {
        my @f0  = $key.split("",:skip-empty);
        my @f1 = @f0.map({ $_ if $_ ~~ /<[\d]>/ });
        for @f1 {
            $iv ~= @f0[$_];
        }
        return $iv;
    } else {
        return $key;
    }
}

sub MAIN( Str :$text = 'hello, world!', Str :$cipher = '%29%d5%5f%fd%80%99%cd%5a%a1%ff%2c%98%20%99%b6%ab') {

    # prints iv and key
    gen_cipher_param('iv').say;
    gen_cipher_param('key').say;

    # cipher text
    my $ciphertext = encrypt($text.encode, :aes256, :iv((gen_cipher_param('iv')).encode), :key((gen_cipher_param('key')).encode));

    # checkpoint
    $ciphertext.say;

    my $c = join( "", map { "%" ~ sprintf "%02x", ord($_) if $_ }, $ciphertext.unpack("A*").split("",:skip-empty) );

    # prints ciphered text as url-encoded string
    $c.say;

    # init ciphered string
    $c = $cipher;

    # decipher string
    my @list = map { :16($_) if $_ ~~ m:Perl5/[0-9A-Za-z]+/ }, $c.split: /\%/, :skip-empty;
    my $buf = buf8.new(@list);

    # checkpoint
    $buf.say;

    my $plaintext = decrypt($buf, :aes256, :iv((gen_cipher_param('iv')).encode), :key((gen_cipher_param('key')).encode));
    my $deciphered = $plaintext.decode;

    # prints deciphered text
    $deciphered.say;

    # generate md5 hash
    my $md5 = md5($deciphered.encode).unpack("H*");
    $md5.say;
}
