# Perl6 crypt experiments

## Overview

Here's some experiments with encrypt/decrypt functions via OpenSSL::CryptTools in Perl 6. Also md5 hash generation example via OpenSSL::Digest is included.

## License information

These scripts are free and opensource software, so you can redistribute them and/or modify them under the terms of the Artistic License 2.0.

## Links and credits

[Pheix brew site](https://perl6.pheix.org)

[OpenSSL in Perl 6](https://github.com/sergot/openssl)

[The Artistic License 2.0](https://opensource.org/licenses/Artistic-2.0)

## Contact

Please contact us via [feedback form](https://pheix.org/feedback.html) at pheix.org
